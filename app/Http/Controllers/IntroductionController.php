<?php

namespace App\Http\Controllers;

use App\DB;
use Illuminate\Http\Request;

class IntroductionController extends Controller
{
    public function index()
    {
        $lessons = DB::table('lessons')->get();
        return view ('lessons.introduction')->with('lessons',$lessons); 
    }
}
