<?php

namespace App\Http\Controllers;

use App\SubTopic;
use App\Topic;
use Illuminate\Http\Request;
use App\Http\Requests\StoreSubTopicsRequest;
use App\Http\Requests\UpdateSubTopicsRequest;

class SubtopicsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subtopics = SubTopic::all();

        return view('subtopics.index', compact('subtopics'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $relations = [
            'topics' => \App\Topic::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];
        return view('subtopics.create', $relations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubTopicsRequest $request)
    {

        SubTopic::create($request->all());

        return redirect()->route('subtopics.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubTopic  $subTopic
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $relations = [
            'topics' => \App\Topic::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $subtopic = SubTopic::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubTopic  $subTopic
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subtopic = SubTopic::findOrFail($id);

        return view('subtopics.edit', compact('subtopic'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubTopic  $subTopic
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubTopicsRequest $request, $id)
    {
        $relations = [
            'topics' => \App\Topic::get()->pluck('title', 'id')->prepend('Please select', ''),
        ];

        $subtopic = SubTopic::findOrFail($id);

        return view('subtopics.show', compact('subtopic') + $relations);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubTopic  $subTopic
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subtopic = SubTopic::findOrFail($id);
        $subtopic->delete();

        return redirect()->route('subtopics.index');
    }

    public function massDestroy(Request $request)
    {
        if ($request->input('ids')) {
            $entries = SubTopic::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }
}
