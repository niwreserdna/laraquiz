<?php
namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Topic
 *
 * @package App
 * @property string $title
*/
class Topic extends Model
{
    use SoftDeletes;

    protected $fillable = ['title', 'topic_id'];

    public static function boot()
    {
        parent::boot();

        Topic::observe(new \App\Observers\UserActionsObserver);
    }

    public function topics()
    {
        Topic::get('topic_id');
    }

    public function questions()
    {
        return $this->hasMany(Question::class, 'topic_id')->withTrashed();
    }
        public function subtopics()
    {
        return $this->hasMany(SubTopic::class, 'subtopic_id')->withTrashed();
    }

    public function lessons()
    {
        return $this->belongsTo(Lesson::class, 'id')->withTrashed();
    }
}
