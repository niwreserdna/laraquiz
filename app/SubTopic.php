<?php

namespace App;

use App\SubTopic;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class SubTopic extends Model
{
    use SoftDeletes;

    protected $table = 'subtopics';
    protected $fillable = ['title', 'subtopic_id','topic_id'];

    public static function boot()
    {
        parent::boot();

        SubTopic::observe(new \App\Observers\UserActionsObserver);
    }

    public function setTopicIdAttribute($input)
    {
        $this->attributes['topic_id'] = $input ? $input : null;
    }

    public function subtopics()
    {
        SubTopic::get('subtopic_id');
    }

    public function topics()
    {
        return $this->belongsTo(Topic::class, 'topic_id')->withTrashed();
    }

    public function lessons()
    {
        return $this->belongsTo(Lesson::class, 'id')->withTrashed();
    }
}
