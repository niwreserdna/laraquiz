<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Lesson extends Model
{
    use SoftDeletes;

    protected $fillable = ['subtopic_id', 'topic_id','lesson_text'];

    public static function boot()
    {
        parent::boot();

        Lesson::observe(new \App\Observers\UserActionsObserver);
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setTopicIdAttribute($input)
    {
        $this->attributes['topic_id'] = $input ? $input : null;
    }

    public function setSubTopicIdAttribute($input)
    {
        $this->attributes['subtopic_id'] = $input ? $input : null;
    }

    public function topic()
    {
        return $this->hasMany(Topic::class, 'topic_id')->withTrashed();
    }

    public function subtopic()
    {
        return $this->hasMany(SubTopic::class, 'subtopic_id')->withTrashed();
    }
}

