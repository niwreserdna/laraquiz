<?php
Route::get('/', function () {
    return view('client-views/try');
});

Route::get('/login', function () {
    return view('auth.login');
});

Route::get('/exercise',function () {
    return view('layouts.app');
});

Route::get('lessoncss/lessonhome', function () {
    return view('lessons.lessonhome');
});

Route::get('lessoncss/introduction', function () {
    return view('lessons.introduction');
});
Route::get('lessoncss/syntax', function () {
    return view('lessons.syntax');
});
Route::get('lessoncss/variable', function () {
    return view('lessons.variable');
});
Route::get('lessoncss/constant', function () {
    return view('lessons.constant');
});
Route::get('lessoncss/dtypes', function () {
    return view('lessons.dtypes');
});
Route::get('lessoncss/operator', function () {
    return view('lessons.operator');
});
Route::get('lessoncss/decision', function () {
    return view('lessons.decision');
});
Route::get('lessoncss/loop', function () {
    return view('lessons.loop');
});
Route::get('lessoncss/fixed', function () {
    return view('lessons.fixed');
});

Route::get('lessoncss/split', function () {
    return view('lessons.split');
});

Route::get('lessoncss/mySQL', function () {
    return view('lessons.mySQL');
});
Route::get('lessoncss/prep', function () {
    return view('lessons.prep');
});
Route::get('lessoncss/record', function () {
    return view('lessons.record');
});
Route::get('lessoncss/dml', function () {
    return view('lessons.dml');
});
Route::get('lessoncss/imp', function () {
    return view('lessons.imp');
});
Route::get('lessoncss/session', function () {
    return view('lessons.session');
});
Route::get('lessoncss/slide', function () {
    return view('lessons.slide');

});
Route::get('lessoncss/practice', function () {
    return view('lessons.practice');
});


Route::get('client/intro2', 'LessonController@lesson2');
Route::get('client/intro', 'LessonController@lesson1');
Route::get('client/intro3', 'LessonController@lesson3');
Route::get('client/chap1', 'LessonController@lesson4');
Route::get('client/chap2', 'LessonController@lesson5');
Route::get('client/chap3', 'LessonController@lesson6');
Route::get('client/chap4', 'LessonController@lesson7');
    /*function () {

    return view('client-views.intro');
});*/
// Auth::routes();

// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('auth.login');
$this->post('login', 'Auth\LoginController@login')->name('auth.login');
$this->get('logout', 'Auth\LoginController@logout')->name('auth.logout');
$this->post('logout', 'Auth\LoginController@logout')->name('auth.logout');
$this->get('oauth2google', 'Auth\Oauth2Controller@oauth2google')->name('oauth2google');
$this->get('googlecallback', 'Auth\Oauth2Controller@googlecallback')->name('googlecallback');
$this->get('oauth2facebook', 'Auth\Oauth2Controller@oauth2facebook')->name('oauth2facebook');
$this->get('facebookcallback', 'Auth\Oauth2Controller@facebookcallback')->name('facebookcallback');
$this->get('oauth2github', 'Auth\Oauth2Controller@oauth2github')->name('oauth2github');
$this->get('githubcallback', 'Auth\Oauth2Controller@githubcallback')->name('githubcallback');

// Registration Routes...
$this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('auth.register');
$this->post('register', 'Auth\RegisterController@register')->name('auth.register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('auth.password.email');
$this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');


    Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index');
    Route::resource('tests', 'TestsController');
    Route::resource('roles', 'RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::resource('users', 'UsersController');
    Route::post('users_mass_destroy', ['uses' => 'UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::resource('user_actions', 'UserActionsController');
    Route::resource('topics', 'TopicsController');
    Route::post('topics_mass_destroy', ['uses' => 'TopicsController@massDestroy', 'as' => 'topics.mass_destroy']);
    Route::resource('questions', 'QuestionsController');
    Route::post('questions_mass_destroy', ['uses' => 'QuestionsController@massDestroy', 'as' => 'questions.mass_destroy']);
    Route::resource('questions_options', 'QuestionsOptionsController');
    Route::post('questions_options_mass_destroy', ['uses' => 'QuestionsOptionsController@massDestroy', 'as' => 'questions_options.mass_destroy']);
    Route::resource('results', 'ResultsController');
    Route::post('results_mass_destroy', ['uses' => 'ResultsController@massDestroy', 'as' => 'results.mass_destroy']);
    Route::resource('subtopics', 'SubtopicsController');
    Route::post('subtopics_mass_destroy', ['uses' => 'SubtopicsController@massDestroy', 'as' => 'subtopics.mass_destroy']);
    Route::resource('lessons', 'LessonController');
    Route::post('lessons_mass_destroy', ['uses' => 'LessonController@massDestroy', 'as' => 'lessons.mass_destroy']);
    Route::get('/exercise', 'ExerciseController@index');
});
