<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title>PHP Online Editor</title>
    <link rel="stylesheet" id="font-awesome"  href="libs/font-awesome.min.css" type="text/css" media="all" />
    <script src="libs/jquery-2.1.4.min.js"></script>    
    
    <link rel="stylesheet"  href="css/php-einfach-online-php-editor.css" type="text/css" media="all" />
    <script type="text/javascript" src="../lessoncss/js/php-einfach-online-php-editor.js"></script>

     <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style2.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

<style>
body {
    font-family: Arial; font-size: 22px;
}

a {
    text-decoration: none;
    color: #20477a;
}

.ace_editor {
    font-size: 14px !important;
}
.ace_editor {
    font-size: 14px !important;

}
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
  
  }
.splits {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}
.left {
  left: 250px;
  width: 300px;
  position: absolute;

  
}
.right {
 position: absolute;
}

</style>
</head>
 
<body>

<div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar" >

            
            <div class="sidebar-header">
                <center>
                <a href="/"  role="button" style="background-color: black;"><h3>PHP Tutorial</h3></a>
                <center>
            </div>

             <ul class="list-unstyled components">
                <li>
                    <a href="lessonhome">
                       Home
                    </a></li>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter I
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="introduction">Introduction</a>
                        </li>
                        <li>
                            <a href="syntax"> Syntax Overview</a>
                        </li>


                    </ul>
               
    

                <li class="active">
                    <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter II
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu2">
                        <li class="btn-info"><strong><i>
                            <a href="variable">Variables</a>
                        </li></i> </strong>
                        <li>
                            <a href="constant"> Constant</a>
                        </li>
                        <li>
                            <a href="dtypes"> Data Types</a>
                        </li>
                        <li>
                            <a href="operator">Operator</a>
                        </li>
                         <li>
                    <a href="decision" >
                        Decision Making
                    </a></li>
                    <li>
                    <a href="loop" >
                        Loop Types
                    </a></li>
                    <li>
                    <a href="mySQL" >
                        My SQL
                    </a></li>
                    </ul>
                </li> </li>

                <li class="active">
                    <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter III
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu3">
                          <li>
                        <a href="prep" >
                        PHP Prep 
                    </a></li>
                      <li>
                        <a href="record" >
                        MySQL Rec 
                    </a></li>
                     <li>
                        <a href="imp" >
                        Import
                    </a></li>
                    <li>
                        <a href="dml" >
                       DML
                    </a></li>
                    <li>
                        <a href="session" >
                       Session
                    </a></li>
                    
                    </ul>
                </li>
                <li>
                        <a href="practice" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Practice</a>
                    </li>
                    <li>
                        <a href="/tests" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Take Exercise</a>
                    </li>
        </nav>

        <!-- Page Content  -->
        <div id="content" class="split left"  style="width: 750px; ">
            <body style= "background: url(https://mbtskoudsalg.com/explore/blue-borders-png/#gal_post_3366_blue-borders-png-4.png); background-size: 100%;">
                
          
           
            
<p><h1>PHP Tutorial: Variables</h1></p><br><br>
Variable is a symbol or name that stands for a value. Variables are used for storing values such as numeric values, characters, character strings, or memory addresses so that they can be used in any part of the program.<br><br>
Variables are used as "containers" in which we store information.<br><br>
A PHP variable starts with a dollar sign ($), which is followed by the name of the variable<br<br>

$variable_name = value;<br>
Rules for PHP variables:<bt>
- A variable name must start with a letter or an underscore<br>
- A variable name cannot start with a number<br>
- A variable name can only contain alpha-numeric characters and underscores (A-z, 0-9, and _ )<br>
- Variable names are case-sensitive ($name and $NAME would be two different variables)<br><br>

For example:
<div  class="code" id="code_1" data-ace-editor-id="1"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_1" >
&lt;?php
$name = 'John';
$age = 25;
echo $name;
</pre></div>

<i>In the example above, notice that we did not have to tell PHP which data type the variable is.
PHP automatically converts the variable to the correct data type, depending on its value.<br>
*Unlike other programming languages, PHP has no command for declaring a variable. It is created the moment you first assign a value to it.</i>

<br><br><h2>Declaring PHP variables</h2>
All variables in PHP start with a $ (dollar) sign followed by the name of the variable.<br><br>
A valid variable name starts with a<br> letter (A-Z, a-z) or underscore (_), followed by any number of letters, numbers, or underscores.<br><br>
If a variable name is more than one word, it can be separated with an underscore (for example $employee_code instead of $employeecode).<br><br>
'$' is a special variable that can not be assigned.
<br><br><h2>PHP is a loosely type language</h2>
In a language such as C, C++, and Java the programmer must declare the name and type of the variable before use it. In PHP the type of the variable does not need to be declared before use it  because types are associated with values rather than variables.<br><br> As a result, a variable can change the type of its value as much as we want.
As previously mentioned you don't need to declare variables or their type before using them in PHP. In the following example, none of the variables are declared before they are used, the fact is $height is floating number and $width is an integer. 
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" >
<pre class="prettyprint notranslate prettyprinted" style="">&lt;?php
$height = 3.5;
$width = 4;
$area=$height*$width;
echo "Area of the rectangle is : $area";
?&gt;</pre></pre>
</div>

<br><br><h2>PHP variables: Assigning by Reference</h2>
PHP (from PHP4) offers another way to assign values to variables: assign by reference. This means that the new variable simply points the original variable. Changes to the new variable affect the original, and vice a verse.
<br><br><h2>PHP variable variables</h2>
You know how to declare variables in PHP. But what if you want the name your variable is a variable itself? In PHP, you have Variable Variables, so you may assign a variable to another variable.<br><br>
In the following example at line no. 2, we declared a variable called $v which stores the value 'var1' and in line no. 4, "var1" is used as the name of a variable by using two dollar signs. i.e. $$v.<br><br>
Therefore there are two variables now. $v which stores the value "var1" where as $$v which stores the value var2. At this point $$v and $var1 are equal, both store the value "var2".
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" >
<pre class="prettyprint notranslate prettyprinted" style="">&lt;?php
$v='var1';
echo $v; // prints var1
$$v = 'var2'; 
echo $$v; // prints var2
echo $var1; // prints var2
?&gt;
</pre></pre>
</div>

<br><br><h2>PHP Variables Scope</h2>
In PHP, variables can be declared anywhere in the script. We declare the variables for a particular scope. There are two types of scope, the local scope where variables are created and accessed inside a function and global scope where variables are created and accessed outside a function.<br><br>
Example:
<div  class="code" id="code_2" data-ace-editor-id="2"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_2" >

&lt;?php
//global scope
$x = 10;
function var_scope()
{
//local scope
$y=20;
echo "The value of x is :  $x "."&lt;br /&gt;";
echo "The value of y is :  $y"."&lt;br /&gt;";
}
var_scope();
echo "The value of x is :  $x"."&lt;br /&gt;";
echo "The value of y is :  $y ";
?&gt;
</pre></div>
<br>In the above script there are two variables $x and $y and a function var_scope(). $x is a global variable since it is declared outside the function and $y is a local variable as it is created inside the function var_scope(). At the end of the script var_scope() function is called, followed by two echo statements.<br><br>
There are two echo statements inside var_scope() function. It prints the value of $y as it is the locally declared and can not prints the value of $x since it is created outside the function.<br><br>
The next statement of the script prints the value of $x since it is global variable i.e. not created inside any function.<br><br>
The last echo statement can not prints the value of $y since it is local variable and it is created inside the function var_scope() function.

<br><br><h2>The global keyword</h2>
We have already learned variables declared outside a function are global. They can be accessed anywhere in the program except within a function.<br><br>
To use these variables inside a function the variables must be declared global in that function. To do this we use the global keyword before the variables.<br>
Consider the following the example :
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" >
<pre class="prettyprint notranslate prettyprinted" style="">&lt;?php
$x=2;
$y=4;
$z=5;
$xyz=0;
function multiple()
{
global $x, $y, $z, $xyz;
$xyz=$x*$y*$z;
}
multiple();
echo $xyz;
?&gt;
</pre></pre>
</div>

<br><i>In the above example $x, $y, $z, $xyz have initialized with 2, 4, 5, 0. Inside the multiple() function we declared $x, $y, $z, $xyz as global. Therefore all reference of each variable will refer to global version. Now call multiple() anywhere in the script and the variable $xyz will print 40 as it is already referred as global.</i>

<br><br><h2>PHP static variables</h2>
Normally when a function terminates, all of its variables loose its values. Sometimes we want  to  hold these values for the further job. Generally, those variables which hold the values are called static variables inside a function. To do this we must write the keyword "static" in front of  those  variables.Consider the following example without a static variable.<br><br><br><b>Example: without static variable:</b><br>
<div  class="code" id="code_3" data-ace-editor-id="3"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_3" >
&lt;?php
function test_variable()
{
$x=1;
echo $x;
$x++;
}
test_variable();
echo "&lt;br&gt;";
test_variable();
echo "&lt;br&gt;";
test_variable();
?&gt;
</pre></div>
<br><i>In the above script the function test_variable() is useless as the last statement $x = $x +1 can not increase the value of $x since every time it is called $x sets to 1 and print 1.</i>
<br><br><b>Example: with static variable</b>
<div  class="code" id="code_4" data-ace-editor-id="4"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_4" >
&lt;?php
function test_count()
{
static $x=1;
echo $x;
$x++;
}
test_count();
echo "&lt;br&gt;";
test_count();
echo "&lt;br&gt;";
test_count();
?&gt;
</pre></div>

<div>
<script type='text/javascript' src='../lessoncss/libs/ace/ace.js'></script>
<script type='text/javascript' src='../lessoncss/libs/FileSaver.js'></script>
<script type='text/javascript' src='..lessoncss/js/php-einfach-online-php-editor.js'></script>

<script>
jQuery('div[data-ace-editor-id]').each(function() {
    var url='http://execute.php-einfach.de:9999/execute.php'; 
    //var url='proxy.php'; // In case the browser supports cross domain requests, you can use this proxy script to forward the requests over your own server
    var language = 'en'; //Choose 'de' for German
    new OnlinePHPEditor(this, language, url);
});
</script>
</div>

            
        </div>
    </div>

     <!-- jQuery CDN - Slim version (=without AJAX) -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
    <div>
    <h1 style="color: black; position: absolute; right: 130px; top: 10px;">Videos</h1>
<video style="width:320px; height:220px; position:relative; right: -1020px; top:50px;" controls >
  <source src="../lessoncss/vid3.mp4" type="video/mp4">
  </video>
</div>
</body>
</html>
