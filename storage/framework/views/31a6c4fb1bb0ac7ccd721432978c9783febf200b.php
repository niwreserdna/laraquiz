<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title>PHP Online Editor</title>
    <link rel="stylesheet" id="font-awesome"  href="libs/font-awesome.min.css" type="text/css" media="all" />
    <script src="libs/jquery-2.1.4.min.js"></script>    
    
    <link rel="stylesheet"  href="css/php-einfach-online-php-editor.css" type="text/css" media="all" />
    <script type="text/javascript" src="../lessoncss/js/php-einfach-online-php-editor.js"></script>

     <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style2.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    
<style>
body {
    font-family: Arial; font-size: 22px;
}

a {
    text-decoration: none;
    color: #20477a;
}

.ace_editor {
    font-size: 14px !important;
}
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
  
  }
.splits {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}
.left {
  left: 250px;
  width: 300px;
  position: absolute;

  
}
.right {
 position: absolute;
}

</style>
</head>
 
<body>

<div class="wrapper">
        <!-- Sidebar  -->
           <nav id="sidebar" >

            <div class="sidebar-header">
                <center>
                <a href="/"  role="button" style="background-color: black;"><h3>PHP Tutorial</h3></a>
                <center>
            </div>

             <ul class="list-unstyled components">
                <li>
                    <a href="lessonhome">
                       Home
                    </a></li>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter I
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="introduction">Introduction</a>
                        </li>
                        <li>
                            <a href="syntax"> Syntax Overview</a>
                        </li>


                    </ul>
               
    

                <li class="active">
                    <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter II
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu2">
                        <li>
                            <a href="variable">Variables</a>
                        </li>
                        <li>
                            <a href="constant"> Constant</a>
                        </li>
                        <li>
                            <a href="dtypes"> Data Types</a>
                        </li>
                        <li  class="btn-info"><strong><i>
                            <a href="operator">Operator</a>
                        </li></i> </strong>
                         <li>
                    <a href="decision" >
                        Decision Making
                    </a></li>
                    <li>
                    <a href="loop" >
                        Loop Types
                    </a></li>
                    <li>
                    <a href="mySQL" >
                        My SQL
                    </a></li>
                    </ul>
                </li> </li>

                <li class="active">
                    <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter III
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu3">
                          <li>
                        <a href="prep" >
                        PHP Prep 
                    </a></li>
                      <li>
                        <a href="record" >
                        MySQL Rec 
                    </a></li>
                     <li>
                        <a href="imp" >
                        Import
                    </a></li>
                    <li>
                        <a href="dml" >
                       DML
                    </a></li>
                    <li>
                        <a href="session" >
                       Session
                    </a></li>
                    
                    </ul>
                </li>
                <li>
                        <a href="practice" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Practice</a>
                    </li>
                    <li>
                        <a href="/tests" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Take Exercise</a>
                    </li>
        </nav>

        <!-- Page Content  -->
        <div id="content" class="split left"  style="width: 750px; ">
            
<p><h1>PHP Tutorial: Operator Types</h1></p><br>
<br><br><b>What is Operator?</b> Simple answer can be given using expression 4 + 5 is equal to 9. Here 4 and 5 are called operands and + is called operator. PHP language supports following type of operators:<br><br>

• Arithmetic Operators<br>
• Comparison Operators<br>
• Logical (or Relational) Operators<br>
• Assignment Operators<br>
• Conditional (or ternary) Operators<br><br>

Lets have a look on all operators one by one.<br>

<br><br><h2>Arithmetic Operators</h2>
<br><br>There are following arithmetic operators supported by PHP language.<br><br>

Assume variable A holds 10 and variable B holds 20 then:<br>
<style>
<table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>
    <table>
  <tr>
    <th>Operator</th>
    <th>Description</th>
    <th>Example</th>
  </tr>
  <tr>
    <td>+</td>
    <td>Adds two operands</td>
    <td>A + B will give 30</td>
  </tr>
  <tr>
    <td>-</td>
    <td>Subtracts second operand from the first</td>
    <td>A - B will give -10</td>
  </tr>
  <tr>
    <td>*</td>
    <td>Multiply both operands</td>
    <td>A * B will give 200 </td>
  </tr>
  <tr>
    <td>/</td>
    <td>Divide numerator by de-numerator</td>
    <td>B / A will give 2</td>
  </tr>
  <tr>
    <td>%</td>
    <td>Modulus Operator and remainder of after an integer division</td>
    <td>B % A will give 0</td>
  </tr>
  <tr>
    <td>++</td>
    <td>Increment operator, increases integer value by one</td>
    <td>A++ will give 11</td>
  </tr>
  <tr>
    <td>--</td>
    <td>Decrement operator, decreases integer value by one</td>
    <td>A-- will give 9</td>
  </tr>
</table>
</style>
</body>

<br><br><h2>Comparison Operators</h2>
<br>There are following comparison operators supported by PHP language<br><br>

Assume variable A holds 10 and variable B holds 20 then. <br>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>
<table>
  <tr>
    <th>Operator</th>
    <th>Description</th>
    <th>Example</th>
  </tr>
  <tr>
    <td>==</td>
    <td>Checks if the value of two operands are equal or not, if yes then condition becomes true.</td>
    <td>(A == B) is not true.</td>
  </tr>
  <tr>
    <td>!=</td>
    <td>Checks if the value of two operands are equal or not, if values are not equal then condition becomes true.</td>
    <td>(A != B) is true.</td>
  </tr>
  <tr>
    <td>></td>
    <td>Checks if the value of left operand is greater than the value of right operand, if yes then condition becomes true.</td>
    <td>(A > B) is not true. </td>
  </tr>
  <tr>
    <td><</td>
    <td>Checks if the value of left operand is less than the value of right operand, if yes then condition becomes true.</td>
    <td>(A < B) is true.</td>
  </tr>
  <tr>
    <td>>=</td>
    <td>Checks if the value of left operand is greater than or equal to the value of right operand, if yes then condition becomes true</td>
    <td>(A >= B) is not true.</td>
  </tr>
  <tr>
    <td><= </td>
    <td>IChecks if the value of left operand is less than or equal to the value of right operand, if yes then condition becomes true.</td>
    <td>(A <= B) is true.</td>
  </tr>
</table>
</body>



<br><br><h2>Logical Operators</h2><br>
There are following logical operators supported by PHP language<br><br>

Assume variable A holds 10 and variable B holds 20 then:<br>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>
<table>
  <tr>
    <th>Operator</th>
    <th>Description</th>
    <th>Example</th>
  </tr>
  <tr>
    <td>and</td>
    <td>Called Logical AND operator. If both the operands are true then condition becomes true.</td>
    <td>(A and B) is true.</td>
  </tr>
  <tr>
    <td>or</td>
    <td>Called Logical OR Operator. If any of the two operands are non zero then condition becomes true.</td>
    <td>((A or B) is true.</td>
  </tr>
  <tr>
    <td>&&</td>
    <td>Called Logical AND operator. If both the operands are non zero then condition becomes true.</td>
    <td>(A && B) is true. </td>
  </tr>
  <tr>
    <td>||</td>
    <td>Called Logical OR Operator. If any of the two operands are non zero then condition becomes true.</td>
    <td>((A || B) is true.</td>
  </tr>
  <tr>
    <td>!</td>
    <td>Called Logical NOT Operator. Use to reverses the logical state of its operand. If a condition is true then Logical NOT operator will make false.</td>
    <td>!(A && B) is false.</td>
  </tr>ss
</table>
</body>

<br><br><h2>Assignment Operators</h2><br>
There are following assignment operators supported by PHP language: <br>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>
<table>
  <tr>
    <th>Operator</th>
    <th>Description</th>
    <th>Example</th>
  </tr>
  <tr>
    <td>=</td>
    <td>Simple assignment operator, Assigns values from right side operands to left side operand</td>
    <td>(C = A + B will assign value of A + B into C</td>
  </tr>
  <tr>
    <td>+=</td>
    <td>Add AND assignment operator, It adds right operand to the left operand and assign the result to left operand</td>
    <td>C += A is equivalent to C = C + A</td>
  </tr>
  <tr>
    <td>-=/td>
    <td>Subtract AND assignment operator, It subtracts right operand from the left operand and assign the result to left operand</td>
    <td>C -= A is equivalent to C = C - A </td>
  </tr>
  <tr>
    <td>*=</td>
    <td>Multiply AND assignment operator, It multiplies right operand with the left operand and assign the result to left operand</td>
    <td>C *= A is equivalent to C = C * A</td>
  </tr>
  <tr>
    <td>/=</td>
    <td>Divide AND assignment operator, It divides left operand with the right operand and assign the result to left operand</td>
    <td>C /= A is equivalent to C = C / A</td>
  </tr>
  <tr>
    <td>%=</td>
    <td>Modulus AND assignment operator, It takes modulus using two operands and assign the result to left operand</td>
    <td>C %= A is equivalent to C = C % A</td>
  </tr>
</table>
</body>

<br><br><h2>Conditional Operator</h2><br>
There is one more operator called conditional operator. This first evaluates an expression for a true or false value and then execute one of the two given statements depending upon the result of the evaluation. The conditional operator has this syntax: <br>

<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>
<table>
  <tr>
    <th>Operator</th>
    <th>Description</th>
    <th>Example</th>
  </tr>
  <tr>
    <td>Conditional Expression</td>
    <td>If Condition is true ? Then value X : Otherwise value Y</td>
  </tr>
</table>
</body>

<br><br><h2>Operators Categories</h2><br>
All the operators we have discussed above can be categorised into following categories: <br><br>

    >>Unary prefix operators, which precede a single operand.<br>

    >>Binary operators, which take two operands and perform a variety of arithmetic and logical operations.<br>

    >>The conditional operator (a ternary operator), which takes three operands and evaluates either the second or third expression, depending on the evaluation of the first expression.<br>

    >>Assignment operators, which assign a value to a variable.<br>

<br><br><h2>Precedence of PHP Operators</h2><br>
Operator precedence determines the grouping of terms in an expression. This affects how an expression is evaluated. Certain operators have higher precedence than others; for example, the multiplication operator has higher precedence than the addition operator −<br><br>

For example x = 7 + 3 * 2; Here x is assigned 13, not 20 because operator * has higher precedence than + so it first get multiplied with 3*2 and then adds into 7.<br><br>

Here operators with the highest precedence appear at the top of the table, those with the lowest appear at the bottom. Within an expression, higher precedence operators will be evaluated first.<br>
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>
<table>
  <tr>
    <th>Operator</th>
    <th>Description</th>
    <th>Example</th>
  </tr>
  <tr>
    <td>Unary</td>
    <td>! ++ --</td>
    <td>Right to left</td>
  </tr>
  <tr>
    <td>Multiplicative</td>
    <td>* / %</td>
    <td>Left to right</td>
  </tr>
  <tr>
    <td>Additive</td>
    <td>+ -</td>
    <td>Left to righ</td>
  </tr>
  <tr>
    <td>Relational</td>
    <td>< <= > >=   </td>
    <td>Left to right</td>
  </tr>
  <tr>
    <td>Equality</td>
    <td>== !=</td>
    <td>Left to right</td>
  </tr>
  <tr>
    <td>Logical AND</td>
    <td>&&</td>
    <td>Left to right</td>
  </tr>
  <tr>
    <td>Logical OR</td>
    <td>||</td>
    <td>Left to right</td>
  </tr>
  <tr>
    <td>Conditional</td>
    <td>?:</td>
    <td>Right to left</td>
  </tr>
  <tr>
    <td>Assignment</td>
    <td>= += -= *= /= %=</td>
    <td>Right to left</td>
  </tr>
</table>
</body>



<script type='text/javascript' src='../lessoncss/libs/ace/ace.js'></script>
<script type='text/javascript' src='../lessoncss/libs/FileSaver.js'></script>
<script type='text/javascript' src='..lessoncss/js/php-einfach-online-php-editor.js'></script>

<script>
jQuery('div[data-ace-editor-id]').each(function() {
    var url='http://execute.php-einfach.de:9999/execute.php'; 
    //var url='proxy.php'; // In case the browser supports cross domain requests, you can use this proxy script to forward the requests over your own server
    var language = 'en'; //Choose 'de' for German
    new OnlinePHPEditor(this, language, url);
});
</script>
</div>

            
        </div>
    </div>

     <!-- jQuery CDN - Slim version (=without AJAX) -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
    <div>
    <h1 style="color: black; position: absolute; right: 130px; top: 10px;">Videos</h1>
<video style="width:320px; height:220px; position:relative; right: -1020px; top:50px;" controls >
  <source src="../lessoncss/vid5.1.mp4" type="video/mp4">
  </video>
  <video style="width:320px; height:220px; position:relative; right: -700px; top:280px;" controls >
  <source src="../lessoncss/vid5.2.mp4" type="video/mp4">
  </video>
  <video style="width:320px; height:220px; position:relative; right: -375px; top:500px;" controls >
  <source src="../lessoncss/vid5.3.mp4" type="video/mp4">
  </video>
  <video style="width:320px; height:220px; position:relative; right: -50px; top:720px;" controls >
  <source src="../lessoncss/vid5.4.mp4" type="video/mp4">
  </video>
</div>
</body>
</html>
