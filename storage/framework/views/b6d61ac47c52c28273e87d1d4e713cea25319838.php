<?php $request = app('Illuminate\Http\Request'); ?>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu"
            data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed="200">

            <li>
                <a>
                    <span class="title">Chapter 1</span>
                    <span class="fa fa-arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Introduction
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Syntax Overview
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a>
                    <span class="title">Chapter 2</span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Variables
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Constant
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Data Types
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Operator
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Decision Making
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Loop Types
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                MySQL
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a>
                    <span class="title">Chapter 3</span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                PHP Prep
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                MySQL Rec
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Import
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                DML
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo e(route('roles.index')); ?>">
                            <i class="fa fa-book"></i>
                            <span class="title">
                                Session
                            </span>
                        </a>
                    </li>
                </ul>
            </li>  

            <li>
                <a href="lessonhome">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">Back</span>
                </a>
            </li>
        </ul>
    </div>
</div>
