<?php $request = app('Illuminate\Http\Request'); ?>
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <ul class="page-sidebar-menu"
            data-keep-expanded="false"
            data-auto-scroll="true"
            data-slide-speed="200">

            <?php if(Auth::user()->isAdmin()): ?>
            <li class="<?php echo e($request->segment(1) == 'topics' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('topics.index')); ?>">
                    <i class="fa fa-gears"></i>
                    <span class="title">Chapters</span>
                </a>
            </li>
            <li class="<?php echo e($request->segment(1) == 'subtopics' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('subtopics.index')); ?>">
                    <i class="fa fa-gears"></i>
                    <span class="title">Topics</span>
                </a>
            </li>
            <li class="<?php echo e($request->segment(1) == 'lessons' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('lessons.index')); ?>">
                    <i class="fa fa-gears"></i>
                    <span class="title">Lessons</span>
                </a>
            </li>   
            <li class="<?php echo e($request->segment(1) == 'questions' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('questions.index')); ?>">
                    <i class="fa fa-gears"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.questions.title'); ?></span>
                </a>
            </li>
            <li class="<?php echo e($request->segment(1) == 'questions_options' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('questions_options.index')); ?>">
                    <i class="fa fa-gears"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.questions-options.title'); ?></span>
                </a>
            </li>
            <?php endif; ?>

            <li class="<?php echo e($request->segment(1) == 'tests' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('tests.index')); ?>">
                    <i class="fa fa-gears"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.test.new'); ?></span>
                </a>
            </li>

            <li class="<?php echo e($request->segment(1) == 'results' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('results.index')); ?>">
                    <i class="fa fa-gears"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.results.title'); ?></span>
                </a>
            </li>

             <?php if(Auth::user()->isAdmin()): ?>
            <li>
                <a href="logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.logout'); ?></span>
                </a>
            </li>
            <?php endif; ?>
        </ul>
    </div>
</div>
<?php echo Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']); ?>

<button type="submit"><?php echo app('translator')->getFromJson('quickadmin.logout'); ?></button>
<?php echo Form::close(); ?>

