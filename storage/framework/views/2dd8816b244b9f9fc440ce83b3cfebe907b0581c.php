<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title>PHP Online Editor</title>
    <link rel="stylesheet" id="font-awesome"  href="libs/font-awesome.min.css" type="text/css" media="all" />
    <script src="libs/jquery-2.1.4.min.js"></script>    
    
    <link rel="stylesheet"  href="css/php-einfach-online-php-editor.css" type="text/css" media="all" />
    <script type="text/javascript" src="../lessoncss/js/php-einfach-online-php-editor.js"></script>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style2.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    
<style>
body {
    font-family: Arial; font-size: 22px;
}

a {
    text-decoration: none;
    color: #20477a;
}

.ace_editor {
    font-size: 14px !important;
  }
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
  
  }
.splits {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}
.left {
  left: 250px;
  width: 300px;
  position: absolute;

  
}
.right {
 position: absolute;
}
</style>
</head>
 
<body>

<div class="wrapper">
        <!-- Sidebar  -->
           <nav id="sidebar" >

            <div class="sidebar-header">
                <center>
                <a href="/"  role="button" style="background-color: black;"><h3>PHP Tutorial</h3></a>
                <center>
            </div>

             <ul class="list-unstyled components">
                <li>
                    <a href="lessonhome">
                       Home
                    </a></li>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter I
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="introduction">Introduction</a>
                        </li>
                        <li>
                            <a href="syntax"> Syntax Overview</a>
                        </li>


                    </ul>
               
    

                <li class="active">
                    <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter II
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu2">
                        <li>
                            <a href="variable">Variables</a>
                        </li>
                        <li>
                            <a href="constant"> Constant</a>
                        </li>
                        <li>
                            <a href="dtypes"> Data Types</a>
                        </li>
                        <li>
                            <a href="operator">Operator</a>
                        </li>
                         <li>
                    <a href="decision" >
                        Decision Making
                    </a></li>
                    <li>
                    <a href="loop" >
                        Loop Types
                    </a></li>
                    <li>
                    <a href="mySQL" >
                        My SQL
                    </a></li>
                    </ul>
                </li> </li>

                <li class="active">
                    <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter III
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu3">
                          <li>
                        <a href="prep" >
                        PHP Prep 
                    </a></li>
                      <li>
                        <a href="record" >
                        MySQL Rec 
                    </a></li>
                     <li>
                        <a href="imp" >
                        Import
                    </a></li>
                    <li>
                        <a href="dml" >
                       DML
                    </a></li>
                    <li>
                        <a href="session" >
                       Session
                    </a></li>
                    
                    </ul>
                </li>
                <li>
                        <a href="practice" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Practice</a>
                    </li>
                    <li>
                        <a href="/tests" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Take Exercise</a>
                    </li>
        </nav>
        <!-- Page Content  -->
    <div id="content" class="split left"  style="width: 750px; ">

            
<p><h1>PHP Tutorial: Adding a record to a MySQL database</h1></p><br>
To add records to a table in your database, you use more or less the same code as previously. The only thing that needs to change is your SQL statement. The steps we're going to be taking are these:<br>
1.  Open a connection to MySQL<br>
2.  Specify the database we want to open<br>
3.  Set up a SQL Statement that can be used to add records to the database table<br>
4.  Use mysqli_query( ) again, but this time to add records to the table<br>
5.  Close the connection<br><br>
 
Set up a SQL Statement to add records to the database<br>
In our previous script, we used some SQL to grab records from our Address Book database table. We then used a While loop to print all the records out. Because we're now going to be adding records to the Address Book table, we need some different SQL. Here's the script. The new line is in blue (The double and single quotes need to be entered exactly, otherwise you'll get errors when you run the code):<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">&lt;?PHP
require '../configure.php'
$database = "addressbook";
$db_handle = mysqli_connect(DB_SERVER, DB_USER, DB_PASS );
$db_found = mysqli_select_db($db_handle, $database );
if ($db_found) {
$SQL = "INSERT INTO tbl_address_book (First_Name, Surname, Address) VALUES ('Paul', 'McCartney', 'Penny Lane')";
$result = mysqli_query($db_handle, $SQL);
mysqli_close($db_handle);
print "Records added to the database";
}
else {
print "Database NOT Found ";
}
?&gt;
</pre></div><br><br>
You met all of this code from the previous section. The only difference is the new SQL statement! What the code does is to set up some variables, open a connection to the database, and then execute the SQL query. Let's have a look at the new, and rather long, statement.<br>

    
<script type='text/javascript' src='../lessoncss/libs/ace/ace.js'></script>
<script type='text/javascript' src='../lessoncss/libs/FileSaver.js'></script>
<script type='text/javascript' src='../lessoncss/js/php-einfach-online-php-editor.js'></script>


<script>
jQuery('div[data-ace-editor-id]').each(function() {
    var url='http://execute.php-einfach.de:9999/execute.php'; 
    //var url='proxy.php'; // In case the browser supports cross domain requests, you can use this proxy script to forward the requests over your own server
    var language = 'en'; //Choose 'de' for German
    new OnlinePHPEditor(this, language, url);
});
</script>



 <!-- jQuery CDN - Slim version (=without AJAX) -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
       
    </script>
    <div>
    <h1 style="color: black; position: absolute; right: 130px; top: 10px;">Videos</h1>
<video style="width:320px; height:220px; position:relative; right: -1020px; top:50px;" controls >
  <source src="../lessoncss/vid2.mp4" type="video/mp4">
  </video>
</div>
</body>
</html>
