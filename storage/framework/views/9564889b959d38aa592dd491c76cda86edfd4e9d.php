<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title>PHP Online Editor</title>
    <link rel="stylesheet" id="font-awesome"  href="libs/font-awesome.min.css" type="text/css" media="all" />
    <script src="libs/jquery-2.1.4.min.js"></script>    
    
    <link rel="stylesheet"  href="css/php-einfach-online-php-editor.css" type="text/css" media="all" />
    <script type="text/javascript" src="../lessoncss/js/php-einfach-online-php-editor.js"></script>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="../client/style4.css">

    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style2.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    
<style>
body {
    font-family: Arial;
}

a {
    text-decoration: none;
    color: #20477a;
}

.ace_editor {
    font-size: 14px !important;
}
.wrapper {
    display: flex;
    width: 100%;
}

#sidebar {
    width: 250px;
    position: fixed;
    top: 0;
    left: 0;
    height: 100vh;
    z-index: 999;
    background: #7386D5;
    color: #fff;
    transition: all 0.3s;
}
</style>
</head>
 
<body>


<div class="wrapper">
        <!-- Sidebar  -->
         <nav id="sidebar">
            <div class="sidebar-header">
                <h3>PHP Tutorial</h3>
                <strong>PHP</strong>
            </div>

            <a href="/" class="btn btn-info btn-block" role="button">Home Page</a>

            <ul class="list-unstyled components">

               <li>
                    <a href="lessonhome">
                       Home
                    </a>
                </li>
                <li>
                    <a href="introduction" >
                      Introduction
                    </a>
                    </li>
                <li>
                    <a href="syntax" >
                        Syntax Overview
                    </a></li>
                     <li>
                    <a href="variable" >
                        Variables
                    </a></li>
                    <li>
                    <a href="constant" >
                        Constants
                    </a></li>
                    <li>
                    <a href="dtypes" >
                        Data Types
                    </a></li>
                    <li>
                    <a href="operator" >
                        Operator Types
                    </a></li>
                     <li>
                    <a href="decision" >
                        Decision Making
                    </a></li>
                    <li>
                    <a href="loop" >
                        Loop Types
                    </a></li>
                  </ul>
                  </ul>
        </nav>



        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Collapse</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    
                </div>
            </nav>
            

            <div class="line"></div>

           <p><h1>PHP Tutorial</h1></p>
            PHP is the most popular scripting language on the web. Without PHP Facebook, Yahoo, Google wouldn't have exist. The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases. PHP is basically used for developing web based software applications. This tutorial helps you to build your base with PHP. <br><br>


<div  class="code" id="code_1" data-ace-editor-id="1"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_1" >
&lt;html&gt;
&lt;head&gt;
&lt;title&gt;PHP Tutorial&lt;/title&gt;
&lt;/head&gt;
&lt;body&gt;
&lt;?php
echo "Welcome to PHP Tutorial. The current time is: ".date("h:i:s")." &lt;br /&gt;";
?&gt;
&lt;/body&gt;
&lt;/html&gt;
</pre>


<script type='text/javascript' src='../lessoncss/libs/ace/ace.js'></script>
<script type='text/javascript' src='../lessoncss/libs/FileSaver.js'></script>
<script type='text/javascript' src='../lessoncss/js/php-einfach-online-php-editor.js'></script>


<script>
jQuery('div[data-ace-editor-id]').each(function() {
    var url='http://execute.php-einfach.de:9999/execute.php'; 
    //var url='proxy.php'; // In case the browser supports cross domain requests, you can use this proxy script to forward the requests over your own server
    var language = 'en'; //Choose 'de' for German
    new OnlinePHPEditor(this, language, url);
});
</script>

</div>




            <div class="line"></div>

            <h2></h2>
            <p></p>

            
        </div>
    </div>

      <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>
</body>
</html>
