<?php $__env->startSection('content'); ?>
    <h3 class="page-title">Lessons</h3>
    <?php echo Form::open(['method' => 'POST', 'route' => ['lessons.store']]); ?>


    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo app('translator')->getFromJson('quickadmin.create'); ?>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <?php echo Form::label('topic_id', 'Chapter*', ['class' => 'control-label']); ?>

                    <?php echo Form::select('topic_id', $topics, old('topic_id'), ['class' => 'form-control']); ?>

                    <p class="help-block"></p>
                    <?php if($errors->has('topic_id')): ?>
                        <p class="help-block">
                            <?php echo e($errors->first('topic_id')); ?>

                        </p>
                    <?php endif; ?>
                </div>
            </div>

            <div class="col-xs-12 form-group">
                    <?php echo Form::label('subtopic_id', 'Topic*', ['class' => 'control-label']); ?>

                    <?php echo Form::select('subtopic_id', $subtopics, old('subtopic_id'), ['class' => 'form-control']); ?>

                    <p class="help-block"></p>
                    <?php if($errors->has('subtopic_id')): ?>
                        <p class="help-block">
                            <?php echo e($errors->first('subtopic_id')); ?>

                        </p>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    <?php echo Form::label('lesson_text', 'Lesson text*', ['class' => 'control-label']); ?>

                    <?php echo Form::textarea('lesson_text', old('lesson_text'), ['class' => 'form-control ', 'placeholder' => '']); ?>

                    <p class="help-block"></p>
                    <?php if($errors->has('lesson_text')): ?>
                        <p class="help-block">
                            <?php echo e($errors->first('lesson_text')); ?>

                        </p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>


    <?php echo Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']); ?>

    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>