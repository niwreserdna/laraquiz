<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title>PHP Online Editor</title>
    <link rel="stylesheet" id="font-awesome"  href="libs/font-awesome.min.css" type="text/css" media="all" />
    <script src="libs/jquery-2.1.4.min.js"></script>    
    
    <link rel="stylesheet"  href="css/php-einfach-online-php-editor.css" type="text/css" media="all" />
    <script type="text/javascript" src="../lessoncss/js/php-einfach-online-php-editor.js"></script>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="../client/style4.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    
<style>
body {
    font-family: Arial;
}

a {
    text-decoration: none;
    color: #20477a;
}

.ace_editor {
    font-size: 14px !important;
}
</style>
</head>
 
<body>

<div class="wrapper">
        <!-- Sidebar  -->
         <nav id="sidebar">
            <div class="sidebar-header">
                <h3>PHP Tutorial</h3>
                <strong>PHP</strong>
            </div>

            <a href="/" class="btn btn-info btn-block" role="button">Home Page</a>

            <ul class="list-unstyled components">

               <li>
                    <a href="lessonhome">
                       Home
                    </a>
                </li>
                <li>
                    <a href="introduction" >
                      Introduction
                    </a>
                    </li>
                <li>
                    <a href="syntax" >
                        Syntax Overview
                    </a></li>
                     <li>
                    <a href="variable" >
                        Variables
                    </a></li>
                    <li>
                    <a href="constant" >
                        Constants
                    </a></li>
                    <li>
                    <a href="dtypes" >
                        Data Types
                    </a></li>
                    <li>
                    <a href="operator" >
                        Operator Types
                    </a></li>
                     <li>
                    <a href="decision" >
                        Decision Making
                    </a></li>
                    <li>
                    <a href="loop" >
                        Loop Types
                    </a></li>
                  </ul>
                  </ul>
        </nav>




        <!-- Page Content  -->
        <div id="content">

            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">

                    <button type="button" id="sidebarCollapse" class="btn btn-info">
                        <i class="fas fa-align-left"></i>
                        <span>Collapse</span>
                    </button>
                    <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <i class="fas fa-align-justify"></i>
                    </button>

                    
                </div>
            </nav>
            
<p><h1>PHP Tutorial: Data Types</h1></p>
            <br><br>Variables can store a variety of data types.
<br><br>Data types supported by PHP: <b>String</b>, <b>Integer</b>,  <b>Float</b>,  <b>Boolean</b>,  <b>Array</b>,  <b>Object</b>,  <b>NULL</b>,  <b>Resource</b>.

<br><br><h2>String</h2>

They are sequences of characters, like "PHP supports string operations". Following are valid examples of string.
For example: 
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="ooo">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">$string_1 = "This is a string in double quotes";
$string_2 = 'This is a somewhat longer, singly quoted string';
$string_39 = "This string has thirty-nine characters";
$string_0 = ""; // a string with zero characters</pre>
</div>

<br><br>Singly quoted strings are treated almost literally, whereas doubly quoted strings replace variables with their values as well as specially interpreting certain character sequences.<br>
<div  class="code" id="code_2" data-ace-editor-id="2"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_2" >
&lt;?php
   $variable = "name";
   $literally = 'My $variable will not print!';
   
   print($literally);
   print "<br>";
   
   $literally = "My $variable will print!";
   print($literally);
?&gt;
</pre></div>

<br><br>There are no artificial limits on string length - within the bounds of available memory, you ought to be able to make arbitrarily long strings.<br><br>

Strings that are delimited by double quotes (as in "this") are preprocessed in both the following two ways by PHP :<br><br>

    --Certain character sequences beginning with backslash (\) are replaced with special characters<br>

    --Variable names (starting with $) are replaced with string representations of their values. <br><br>

The escape-sequence replacements are: <br><br>

--\n is replaced by the newline character<br>
--\r is replaced by the carriage-return character<br>
--\t is replaced by the tab character<br>
--\$ is replaced by the dollar sign itself ($)<br>
--\"is replaced by a single double-quote (")<br>
--\\ is replaced by a single backslash (\)<br>



<br><br><h2>Integer</h2><br>

An integer is a whole number (without decimals) that must fit the following criteria:<br>
- It cannot contain commas or blanks<br>
- It must not have a decimal point<br>
- It can be either positive or negative<br>

<br>For example: 
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">  &lt;?php
$int1 = 42; // positive number
$int2 = -42; // negative number
?&gt;</pre>
</div>

<br><br><h2>Double</h2><br>

They like 3.14159 or 49.1. By default, doubles print with the minimum number of decimal places needed. <br>For example:
<div  class="code" id="code_3" data-ace-editor-id="3"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_3" >
<?php
   $many = 2.2888800;
   $many_2 = 2.2111200;
   $few = $many + $many_2;
   
   print("$many + $many_2 = $few <br>");
?>
</pre></div>


<br><br><h2>Boolean</h2>

A Boolean represents two possible states: TRUE or FALSE
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">  &lt;?php
$x = true; $y = false;
?&gt;
</pre>
</div>

Booleans are often used in conditional testing, which will be covered later on in the course.<br><br>
Most of the data types can be used in combination with one another. In this example, string and integer are put together to determine the sum of two numbers.
<br><br><br>For example:<br>
<div  class="code" "
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" " >
&lt;?php
$str = "10";
$int = 20;
$sum = $str + $int;
echo ($sum);
</pre></div>
<br><i>PHP automatically converts each variable to the correct data type, according to its value. This is why the variable $str is treated as a number in the addition.</i>

<br>Can also be used like:
<br><br><br>For example:<br>
<div  class="code"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" >
if (TRUE)
   print("This will always print<br>");

else
   print("This will never print<br>");
</pre></div>
<br><br><h2>Interpreting other types as Booleans</h2>
>>Here are the rules for determine the "truth" of any value not already of the Boolean type:<br>

>>If the value is a number, it is false if exactly equal to zero and true otherwise.<br>

>>If the value is a string, it is false if the string is empty (has zero characters) or is the string "0", and is true otherwise.<br>

>>Values of type NULL are always false.<br>

>>If the value is an array, it is false if it contains no other values, and it is true otherwise. For an object, containing a value means having a member variable that has been assigned a value.<br>

>>Valid resources are true (although some functions that return resources when they are successful will return FALSE when unsuccessful).<br>

>>Don't use double as Booleans.<br>

Each of the following variables has the truth value embedded in its name when it is used in a Boolean context.<br>
<div  class="code"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" >
$true_num = 3 + 0.14159;
$true_str = "Tried and true"
$true_array[49] = "An array element";
$false_array = array();
$false_null = NULL;
$false_num = 999 - 999;
$false_str = "";
</pre></div>

<br><br><h2>Null</h2>
NULL is a special type that only has one value: NULL. To give a variable the NULL value, simply assign it like this:
<div  class="code"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" >
$my_var = NULL;
</pre></div>

<br>The special constant NULL is capitalized by convention, but actually it is case insensitive; you could just as well have typed: 
<div  class="code"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" >
$my_var = null;
</pre></div>

<br><br> A variable that has been assigned NULL has the following properties:

--It evaluates to FALSE in a Boolean context.

--It returns FALSE when tested with IsSet() function.



<script type='text/javascript' src='../lessoncss/libs/ace/ace.js'></script>
<script type='text/javascript' src='../lessoncss/libs/FileSaver.js'></script>
<script type='text/javascript' src='..lessoncss/js/php-einfach-online-php-editor.js'></script>

<script>
jQuery('div[data-ace-editor-id]').each(function() {
    var url='http://execute.php-einfach.de:9999/execute.php'; 
    //var url='proxy.php'; // In case the browser supports cross domain requests, you can use this proxy script to forward the requests over your own server
    var language = 'en'; //Choose 'de' for German
    new OnlinePHPEditor(this, language, url);
});
</script>
</div>

    

            
        </div>
    </div>

      <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
<a href="/tests" class="btn btn-info btn-lg btn-dark" role="button">Take Exercise</a>
</body>
</html>
