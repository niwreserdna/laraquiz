<?php $__env->startSection('content'); ?>
    <h3 class="page-title">Lessons</h3>
    
    <?php echo Form::model($lesson, ['method' => 'PUT', 'route' => ['lessons.update', $lesson->id]]); ?>


    <div class="panel panel-default">
        <div class="panel-heading">
            <?php echo app('translator')->getFromJson('quickadmin.edit'); ?>
        </div>

        <div class="row">
                <div class="col-xs-12 form-group">
                    <?php echo Form::label('lesson_text', 'Lesson text*', ['class' => 'control-label']); ?>

                    <?php echo Form::textarea('lesson_text', old('lesson_text'), ['class' => 'form-control ', 'placeholder' => '']); ?>

                    <p class="help-block"></p>
                    <?php if($errors->has('lesson_text')): ?>
                        <p class="help-block">
                            <?php echo e($errors->first('lesson_text')); ?>

                        </p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <?php echo Form::label('code_snippet', 'Code snippet', ['class' => 'control-label']); ?>

                    <?php echo Form::textarea('code_snippet', old('code_snippet'), ['class' => 'form-control ', 'placeholder' => '']); ?>

                    <p class="help-block"></p>
                    <?php if($errors->has('code_snippet')): ?>
                        <p class="help-block">
                            <?php echo e($errors->first('code_snippet')); ?>

                        </p>
                    <?php endif; ?>
                </div>
            </div>
            
        </div>
    </div>

    <?php echo Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']); ?>

    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>