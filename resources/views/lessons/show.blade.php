@extends('layouts.app')

@section('content')
    <h3 class="page-title">Lessons</h3>
    
    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.view')
        </div>
        
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Chapter</th>
                    <td>{{ $lessons->topic->title}}</td></tr>
                        <tr>
                            <th>Topic</th>
                    <td>{{ $lessons->subtopic_id }}</td></tr>
                        <tr>
                            <th>Code Snippets</th>
                    <td>{{ $lessons->code_snippets }}</td></tr>
                    </table>
                </div>
            </div>

            <p>&nbsp;</p>

            <a href="{{ route('lessons.index') }}" class="btn btn-default">@lang('quickadmin.back_to_list')</a>
        </div>
    </div>
@stop