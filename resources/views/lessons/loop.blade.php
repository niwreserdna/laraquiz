<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title>PHP Online Editor</title>
    <link rel="stylesheet" id="font-awesome"  href="libs/font-awesome.min.css" type="text/css" media="all" />
    <script src="libs/jquery-2.1.4.min.js"></script>    
    
    <link rel="stylesheet"  href="css/php-einfach-online-php-editor.css" type="text/css" media="all" />
    <script type="text/javascript" src="../lessoncss/js/php-einfach-online-php-editor.js"></script>

    <!-- Bootstrap CSS CDN -->
     <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style2.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    
<style>
body {
    font-family: Arial; font-size: 22px;
}

a {
    text-decoration: none;
    color: #20477a;
}

.ace_editor {
    font-size: 14px !important;
}
 .split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
  
  }
.splits {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}
.left {
  left: 250px;
  width: 300px;
  position: absolute;

  
}
.right {
 position: absolute;
}
</style>
</head>
 
<body>

<div class="wrapper">
        <!-- Sidebar  -->
         <nav id="sidebar" >

            
            <div class="sidebar-header">
                <center>
                <a href="/"  role="button" style="background-color: black;"><h3>PHP Tutorial</h3></a>
                <center>
            </div>

             <ul class="list-unstyled components">
                <li>
                    <a href="lessonhome">
                       Home
                    </a></li>
                <li class="active">
                        Chapter I
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="introduction">Introduction</a>
                        </li>
                        <li>
                            <a href="syntax"> Syntax Overview</a>
                        </li>


                    </ul>
               
    

                <li class="active">
                    <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter II
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu2">
                        <li>
                            <a href="variable">Variables</a>
                        </li>
                        <li>
                            <a href="constant"> Constant</a>
                        </li>
                        <li>
                            <a href="dtypes"> Data Types</a>
                        </li>
                        <li>
                            <a href="operator">Operator</a>
                        </li>
                         <li>
                    <a href="decision" >
                        Decision Making
                    </a></li>
                    <li class="btn-info"><strong><i>
                    <a href="loop" >
                        Loop Types
                    </a></li></i> </strong>
                    <li>
                    <a href="mySQL" >
                        My SQL
                    </a></li>
                    </ul>
                </li> </li>

                <li class="active">
                    <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter III
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu3">
                          <li>
                        <a href="prep" >
                        PHP Prep 
                    </a></li>
                      <li>
                        <a href="record" >
                        MySQL Rec 
                    </a></li>
                     <li>
                        <a href="imp" >
                        Import
                    </a></li>
                    <li>
                        <a href="dml" >
                       DML
                    </a></li>
                    <li>
                        <a href="session" >
                       Session
                    </a></li>
                    
                    </ul>
                </li>
                <li>
                        <a href="practice" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Practice</a>
                    </li>
                    <li>
                        <a href="/tests" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Take Exercise</a>
                    </li>
        </nav>
        <!-- Page Content  -->
       <div id="content" class="split left"  style="width: 750px; ">

            
<p><h1>PHP Tutorial: Loop Types</h1></p><br><br>
Loops in PHP are used to execute the same block of code a specified number of times. PHP supports following four loop types.<br><br>

• <b>for </b>− loops through a block of code a specified number of times.<br><br>

• <b>while </b>− loops through a block of code if and as long as a specified condition is true.<br>

• <b>do...while</b> − loops through a block of code once, and then repeats the loop as long as a special condition is true.<br>

• <b>foreach</b> − loops through a block of code for each element in an array.<br>

We will discuss about continue and break keywords used to control the loops execution.

<br><br><h2>The for loop statement</h2><br>
The for statement is used when you know how many times you want to execute a statement or a block of statements.<br>
<img class="img-fluid" src="../client/img/loop.jpg" alt=""><br><br>

Syntax:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor"  >
<pre class="prettyprint notranslate prettyprinted" style="">for (initialization; condition; increment){
   code to be executed;
}</pre>
</div>

The initializer is used to set the start value for the counter of the number of loop iterations. A variable may be declared here for this purpose and it is traditional to name it $i.<br><br>

<h3>Example</h3>
The following example makes five iterations and changes the assigned value of two variables on each pass of the loop:<br>
<div  class="code" id="code_1" data-ace-editor-id="1"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_1" >
  <pre class="prettyprint notranslate prettyprinted" style=""> &lt;html&gt;
   &lt;body&gt;
      
      <?php
         $a = 0;
         $b = 0;
         
         for( $i = 0; $i<5; $i++ ) {
            $a += 10;
            $b += 5;
         }
         
         echo ("At the end of the loop a = $a and b = $b" );
      ?>
   
  &lt;body&gt;
 &lt;html&gt;
</pre></div>

<br><br><h2>The while loop statement</h2><br>

The while statement will execute a block of code if and as long as a test expression is true.<br>

If the test expression is true then the code block will be executed. After the code has executed the test expression will again be evaluated and the loop will continue until the test expression is found to be false.<br>

<br<img class="img-fluid" src="../client/img/while.jpg" alt=""><br><br>

Syntax:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor"  >
<pre class="prettyprint notranslate prettyprinted" style="">while (condition) {
   code to be executed;}</pre>
</div>

<br><br><h3>Example</h3><br>
This example decrements a variable value on each iteration of the loop and the counter increments until it reaches 10 when the evaluation is false and the loop ends.<br>

<div  class="code" id="code_2" data-ace-editor-id="2"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_2" >
     &lt;html&gt;
   &lt;body&gt;
   
      &lt;?php
         $i = 0;
         $num = 50;
    
     
         $i = 0;
         $num = 50;
         
         while( $i < 10) {
            $num--;
            $i++;
         }
         
         echo ("Loop stopped at i = $i and num = $num" );

   ?&gt;
   &lt;/body&gt;
&lt;/html&gt;
</pre></div>

<br><br><h2>The do...while loop statement</h2><br>
The do...while statement will execute a block of code at least once - it then will repeat the loop as long as a condition is true.<br><br>

Syntax:
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor"  >
<pre class="prettyprint notranslate prettyprinted" style="">do {
   code to be executed;
}
while (condition);</pre>
</div>

<br><br><h3>Example</h3><br>
The following example will increment the value of i at least once, and it will continue incrementing the variable i as long as it has a value of less than 10<br>
<div  class="code" id="code_3" data-ace-editor-id="3"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" id="code_editor_3" >
    &lt;html&gt;
   &lt;body&gt;
   
      &lt;?php
         $i = 0;
         $num = 0;
         
         do {
            $i++;
         }
         
         while( $i < 10 );
         echo ("Loop stopped at i = $i" );
      ?&gt;
      
  &lt;/body&gt;
   
   &lt;/html&gt;
</pre></div>

<br><br><h2>The foreach loop statement</h2><br>
The foreach statement is used to loop through arrays. For each pass the value of the current array element is assigned to $value and the array pointer is moved by one and in the next pass next element will be processed.<br><br>

Syntax:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor"  >
<pre class="prettyprint notranslate prettyprinted" style="">foreach (array as value) {
   code to be executed;}</pre>
</div>

<br><br><h3>Example</h3><br>
Try out following example to list out the values of an array<br>
<div  class="code" id="code_4" data-ace-editor-id="4"
  data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false"
  data-ace-editor-script-name="getpost.php" data-ace-editor-default-get="variable1=value1&variable2=value2" data-ace-editor-default-post='"var1":"value1","var2":"value2"'>
<pre class="editor" id="code_editor_4" >
   &lt;html&gt;
     &lt;body&gt;
   
      &lt; ?php
         $array = array( 1, 2, 3, 4, 5);
         
         foreach( $array as $value ) {
            echo "Value is $value <br />";
         }
      
      ?&gt;
      
  &lt;/body&gt;
   
   &lt;/html&gt;
</pre></div>















  

<script type='text/javascript' src='../lessoncss/libs/ace/ace.js'></script>
<script type='text/javascript' src='../lessoncss/libs/FileSaver.js'></script>
<script type='text/javascript' src='..lessoncss/js/php-einfach-online-php-editor.js'></script>

<script>
jQuery('div[data-ace-editor-id]').each(function() {
    var url='http://execute.php-einfach.de:9999/execute.php'; 
    //var url='proxy.php'; // In case the browser supports cross domain requests, you can use this proxy script to forward the requests over your own server
    var language = 'en'; //Choose 'de' for German
    new OnlinePHPEditor(this, language, url);
});
</script>
</div>


            
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
     <div>
    <h1 style="color: black; position: absolute; right: 130px; top: 10px;">Videos</h1>
<video style="width:320px; height:220px; position:relative; right: -1020px; top:50px;" controls >
  <source src="../lessoncss/vid5.1.mp4" type="video/mp4">
  </video>
</div>
</body>
</html>