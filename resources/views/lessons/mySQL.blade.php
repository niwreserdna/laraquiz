<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title>PHP Online Editor</title>
    <link rel="stylesheet" id="font-awesome"  href="libs/font-awesome.min.css" type="text/css" media="all" />
    <script src="libs/jquery-2.1.4.min.js"></script>    
    
    <link rel="stylesheet"  href="css/php-einfach-online-php-editor.css" type="text/css" media="all" />
    <script type="text/javascript" src="../lessoncss/js/php-einfach-online-php-editor.js"></script>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style2.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>


    
<style>
body {
    font-family: Arial; font-size: 22px;
}

a {
    text-decoration: none;
    color: #20477a;
}
.ace_editor {
    font-size: 14px !important;
}
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
  
  }
.splits {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}
.left {
  left: 250px;
  width: 300px;
  position: absolute;

  
}
.right {
 position: absolute;
}
</style>
</head>
 
<body>

<div class="wrapper">
        <!-- Sidebar  -->
           <nav id="sidebar" >

            <div class="sidebar-header">
                <center>
                <a href="/"  role="button" style="background-color: black;"><h3>PHP Tutorial</h3></a>
                <center>
            </div>

             <ul class="list-unstyled components">
                <li>
                    <a href="lessonhome">
                       Home
                    </a></li>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter I
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="introduction">Introduction</a>
                        </li>
                        <li>
                            <a href="syntax"> Syntax Overview</a>
                        </li>


                    </ul>
               
    

                <li class="active">
                    <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter II
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu2">
                        <li>
                            <a href="variable">Variables</a>
                        </li>
                        <li>
                            <a href="constant"> Constant</a>
                        </li>
                        <li>
                            <a href="dtypes"> Data Types</a>
                        </li>
                        <li>
                            <a href="operator">Operator</a>
                        </li>
                         <li>
                    <a href="decision" >
                        Decision Making
                    </a></li>
                    <li>
                    <a href="loop" >
                        Loop Types
                    </a></li>
                    <li class="btn-info"><strong><i>
                    <a href="mySQL" >
                        My SQL
                    </a></li></i> </strong>
                    </ul>
                </li> </li>

                <li class="active">
                    <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter III
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu3">
                          <li>
                        <a href="prep" >
                        PHP Prep 
                    </a></li>
                      <li>
                        <a href="record" >
                        MySQL Rec 
                    </a></li>
                     <li>
                        <a href="imp" >
                        Import
                    </a></li>
                    <li>
                        <a href="dml" >
                       DML
                    </a></li>
                    <li>
                        <a href="session" >
                       Session
                    </a></li>
                    
                    </ul>
                </li>
                <li>
                        <a href="practice" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Practice</a>
                    </li>
                    <li>
                        <a href="/tests" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Take Exercise</a>
                    </li>
        </nav>

        <!-- Page Content  -->
       <div id="content" class="split left"  style="width: 750px; ">

            
<p><h1>PHP Tutorial: MYSQL</h1></p>
<h2>How to connect to MySQL using PHP</h2>


<br><br><p>CONNECTING TO MYSQL USING THE MYSQL IMPROVED EXTENSION
The MySQL Improved extension uses the mysqli class, which replaces the set of legacy MySQL functions.
To connect to MySQL using the MySQL Improved extension, follow these steps:<br><br>
1.  Use the following PHP code to connect to MySQL and select a database. Replace username with your username, password with your password, and dbname with the database name:</p><br><br>

<div  class="code"
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor"  >
  &lt;?php
    $mysqli = new mysqli("localhost", "username", "password", "dbname");
?&gt;
</pre></div>
<br>
<br>
2.  After the code connects to MySQL and selects the database, you can run SQL queries and perform other operations. For example, the following PHP code runs a SQL query that extracts the last names from the employees table, and stores the result in the $result variable:
<br>
<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" >
  &lt;?php
    $result = $mysqli-&gt;query("SELECT lastname FROM employees");
?&gt;
</pre></div><br><br>

Creating a Database in mySql<br><br>
You can create all of your database tables and queries using PHP code. But before doing that, it's a good idea to get an understanding of just what it is you'll be creating. If you're new to the world of databases, then here's a simple primer.<br><br>

What is a database and what do they look like?<br>
A database is a way to store lots of information. You might want to store the names and addresses of all your contacts, or save usernames and passwords for your online forum. Or maybe customer information.<br><br>
When you create a database, you're creating a structure like this:
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>
<table>
  <tr>
    <th>ID</th>
    <th>Title</th>
    <th>First_Name</th>
    <th>Surname</th>
  </tr>
  <tr>
    <td>1</td>
    <td>Mr</td>
    <td>Test</td>
    <td>Name</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Mrs</td>
    <td>Second</td>
    <td>Test</td>
  </tr>
</table>
</body>

The columns (ID, Title, First_Name, Surname) are called <b>Fields</b>. The rows are called <b>Records</b>. Each record is a separate entry.<br><br>
In a database, you save the information in a <b>Table</b>. A single database can contain many tables, and they can be linked together. When the tables are linked together, it's said to be a <b>relational</b> database. If you just have a single table in your database, then it's called a <b>flat-file</b> database. Flat-file database are easier to create and understand, so we'll start by creating one of these using phpMyAdmin.<br>
So start your server and bring up phpMyAdmin, if you havaen't already. Log in with your username and password (the password is blank, if you haven't changed anything.)<br><br>
Although it looks a bit muddled, the part to concentrate on is the textbox under the words create new database, as in the next image:<br>
<img class="img-fluid" src="../client/img/1.jpg" alt=""><br><br>
If you can't see a section "Create new database", click New on the left-hand side of phpMyAdmin:<br>
<img class="img-fluid" src="../client/img/2.jpg" alt=""><br><br>
Type a name for your database, where it says "Create database". <br>Type <b>addressbook</b>:
<img class="img-fluid" src="../client/img/3.jpg" alt=""><br>
<img class="img-fluid" src="../client/img/4.jpg" alt="" ><br><br>
After you have typed a name for your new database, click the "Create" button. You will be taken to a new area.<br>
<img class="img-fluid" src="../client/img/5.jpg" alt=""><br>
In this new area, you can create a Table to go in your database. At the moment, as it says, there are <b>No tables found in the database</b>. But the database itself has been created.<br>
To create a new table, type a name for it in the box at the bottom. You can also type a number for the Fieldstextbox. The fields are the columns, remember, and will be things like first_name, surname, address, etc. You can always add more later, but just type 4 in there. In fact, type it out exactly as it is below:<br>
<img class="img-fluid" src="../client/img/6.jpg" alt=""><br>
<img class="img-fluid" src="../client/img/7.jpg" alt=""><br>
When you've finished, click the <b>Go</b> button. Another, more complex, area will appear:<br>
<img class="img-fluid" src="../client/img/8.jpg" alt="" positio><br>
In this new area, you set up the <b>fields</b> in your database. You can specify whether a field is for text, for numbers, for yes/no values, etc.<br><br>

<h2>Adding/Inserting Records to MySQL table</h2>
To insert a new record into the table you created in the previous section, select the <b>Insert</b> link at the top of the page:<br>
<img class="img-fluid" src="../client/img/9.jpg" alt=""><br><br>

When you click on Insert, you'll be taken to a new area. This one:<br>
<img class="img-fluid" src="../client/img/10.jpg" alt=""><br><br>

As you can see, our four fields are there: ID, First_Name, Surname, and Address. But look at the lengths of the textboxes under the <b>Value</b>. The sizes are determined by the length of the Fields. <br>The address area is a lot bigger, because we used TINYTEXT.<br>
To enter a new record in your table, you type your data in the textboxes under the Value heading. Go ahead and enter the following information for the <b>Value</b> textboxes:<br>
                <b>ID</b>: 1<br>
                <b>First_Name</b>: Test<br>
                <b>Surname</b>: Name<br>
                <b>Address</b>: 12 Test Street <br><br>

Your screen should then look like this:<br>
<img class="img-fluid" src="../client/img/11.jpg" alt=""><br><br>

Finally, click the Go button at the bottom of the screen to create the Row in your table. You will be returned to the Structure screen.<br><br>
And that's it - you now have a database to work with. To see where it has been saved, navigate to your PHP folder on your hard drive. Double click the folder called mysql. Inside this folder will be one called data. This is where all of your databases are stored:<br>
<img class="img-fluid" src="../client/img/12.jpg" alt=""><br><br>
Notice the top folder name in the image above: addressbook. This is the same as the database name, and is automatically created for you for all new databases. When you double click this folder, you should see a few files there:<br>
<img class="img-fluid" src="../client/img/13.jpg" alt=""><br><br>
Notice the files names - they are the same as the tables you create. In other words, they ARE the tables.<br><br>
If you have PHP web space, you can upload this folder and its contents to your data folder, and you should then be able to access the tables in the database with PHP code.<br><br>
We can move on to doing just that - accessing this database with some PHP code.








            

<script type='text/javascript' src='../lessoncss/libs/ace/ace.js'></script>
<script type='text/javascript' src='../lessoncss/libs/FileSaver.js'></script>
<script type='text/javascript' src='..lessoncss/js/php-einfach-online-php-editor.js'></script>

<script>
jQuery('div[data-ace-editor-id]').each(function() {
    var url='http://execute.php-einfach.de:9999/execute.php'; 
    //var url='proxy.php'; // In case the browser supports cross domain requests, you can use this proxy script to forward the requests over your own server
    var language = 'en'; //Choose 'de' for German
    new OnlinePHPEditor(this, language, url);
});
</script>
</div>


            
        </div>
    </div>

     <!-- jQuery CDN - Slim version (=without AJAX) -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
     <div>
    <h1 style="color: black; position: absolute; right: 130px; top: 10px;">Videos</h1>
<video style="width:320px; height:220px; position:relative; right: -1020px; top:50px;" controls >
  <source src="../lessoncss/vid5.1.mp4" type="video/mp4">
  </video>
</div>
</body>
</html>