<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
    <title>PHP Online Editor</title>
    <link rel="stylesheet" id="font-awesome"  href="libs/font-awesome.min.css" type="text/css" media="all" />
    <script src="libs/jquery-2.1.4.min.js"></script>    
    
    <link rel="stylesheet"  href="css/php-einfach-online-php-editor.css" type="text/css" media="all" />
    <script type="text/javascript" src="../lessoncss/js/php-einfach-online-php-editor.js"></script>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link rel="stylesheet" href="style2.css">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.css">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    
<style>
body {
    font-family: Arial; font-size: 22px;
}

a {
    text-decoration: none;
    color: #20477a;
}


.ace_editor {
    font-size: 14px !important;
}
.split {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
  
  }
.splits {
  height: 100%;
  width: 50%;
  position: fixed;
  z-index: 1;
  top: 0;
  overflow-x: hidden;
  padding-top: 20px;
}
.left {
  left: 250px;
  width: 300px;
  position: absolute;

  
}
.right {
 position: absolute;
}
</style>
</head>
 
<body>

<div class="wrapper">
        <!-- Sidebar  -->
          <nav id="sidebar" >

            <div class="sidebar-header">
                <center>
                <a href="/"  role="button" style="background-color: black;"><h3>PHP Tutorial</h3></a>
                <center>
            </div>

             <ul class="list-unstyled components">
                <li>
                    <a href="lessonhome">
                       Home
                    </a></li>
                <li class="active">
                    <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter I
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu">
                        <li>
                            <a href="introduction">Introduction</a>
                        </li>
                        <li>
                            <a href="syntax"> Syntax Overview</a>
                        </li>


                    </ul>
               
    

                <li class="active">
                    <a href="#homeSubmenu2" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter II
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu2">
                        <li>
                            <a href="variable">Variables</a>
                        </li>
                        <li>
                            <a href="constant"> Constant</a>
                        </li>
                        <li>
                            <a href="dtypes"> Data Types</a>
                        </li>
                        <li>
                            <a href="operator">Operator</a>
                        </li>
                         <li>
                    <a href="decision" >
                        Decision Making
                    </a></li>
                    <li>
                    <a href="loop" >
                        Loop Types
                    </a></li>
                    <li>
                    <a href="mySQL" >
                        My SQL
                    </a></li>
                    </ul>
                </li> </li>

                <li class="active">
                    <a href="#homeSubmenu3" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"> 
                        Chapter III
                    </a>
                    <ul class="collapse list-unstyled" id="homeSubmenu3">
                          <li class="btn-info"><strong><i>
                        <a href="prep" >
                        PHP Prep 
                    </a></li></i> </strong>
                      <li>
                        <a href="record" >
                        MySQL Rec 
                    </a></li>
                     <li>
                        <a href="imp" >
                        Import
                    </a></li>
                    <li>
                        <a href="dml" >
                       DML
                    </a></li>
                    <li>
                        <a href="session" >
                       Session
                    </a></li>
                    
                    </ul>
                </li>
                <li>
                        <a href="practice" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Practice</a>
                    </li>
                    <li>
                        <a href="/tests" class="btn btn-info btn-lg btn-dark" role="button" style="width: 250px;">Take Exercise</a>
                    </li>
        </nav>

        <!-- Page Content  -->
       <div id="content" class="split left"  style="width: 750px; ">
            
<p><h1>PHP Prepared Statements</h1></p>
            <br>In a previous section, you saw one way to add a new record to the database. We used the INSERT statement to do this:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL = "INSERT INTO tbl_address_book (First_Name, Surname, Address) VALUES ('Paul', 'McCartney', 'Penny Lane')";</pre>
</div> <br>

However, the above SQL uses hard-coded values ('Paul', 'McCartney', 'Penny Lane'). What usually happens, though, is that you have textboxes where people can enter details. When a button is clicked these VALUES are then used in a SQL statement to add a new record. Before we insert a new record, though, we'll explain Prepared Statements.<br><br>
So far, we've used SQL that pulled all the records from a database, or SQL that inserted hard-coded values like 'Paul' and 'McCartney'. More often than not, however, this is not what you want to do. What usually happens is that you have textboxes where people can enter details, like usernames, passwords, and email addresses. You then get these values from a HTML Form and do something with them. In this lesson, you'll see how to pull records from a database based on an email typed into a textbox. For this, we'll use something called a Prepared Statement.<br><br>

<b>Prepared Statements</b><br><br>
When you ask users to enter details into a textbox and click a button, you are opening your database up to attacks, especially from something called a SQL Injection attack. <br><br>For example, take this SQL Statement:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL = "SELECT * FROM members WHERE email = '$email' ";</pre>
</div><br><br>
Here, we're selecting all the records from a database table called members. The SQL this time has a WHERE clause added. The WHERE clause is used when you want to limit the results to only records that you need. After the word "WHERE", you type a column name from your database (email, in our case). You then have an equals sign, followed by the value you want to check. The value we want to check is coming from the variable called $email. This is surrounded with single quotes.<br>
When an email address is entered in the text box on our form, this value goes straight into the variable without any checks. An attacker could type something else into the textbox, trying to manipulate your SQL statement. They could add a DELETE part to delete records from your database, or a DROP clause to delete the entire database itself. There are lots of ways an attacker could inject SQL into your code. So you need to defend yourself against attacks.<br><br>
To prevent SQL Injection attacks like these, you can use a Prepared Statement. Let's see how they work.<br><br>
<b>The PHP Scripts</b><br><br>
Along with the database folder amongst the files you downloaded, there is a PHP script called testPrep.php (in the scripts folder). We'll use this script, and the database, to teach you about prepared statements.<br>
Open up the testPrep.php file and you'll see some PHP at the top and a FORM in the BODY section of the HTML.<br> The form is just this:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> &lt;FORM NAME ="form1" METHOD ="POST" ACTION ="testPrep.php"&gt;
email address &lt;INPUT TYPE = 'TEXT' Name ='email' value="&lt;?PHP print $email ; ?&gt;"&gt;
</pre></div><br><br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> &lt;INPUT TYPE = "Submit" Name = "Submit1" VALUE = "Login"&gt;
</pre>
</div><br><br>

<b>&lt;/FORM&gt;</b><br><br>
It's a simple form, with a textbox and a button. The textbox is for an email address. When a correct email address is entered, we'll print out a row from the database. The row contains an ID number, a username, a password, and the email address. So we're querying the database table to see if the email address entered into the textbox matches an email address from the table.<br><br>
In fact, test it out. Load up the testPrep.php into your web browser. Enter the following email address into the textbox:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> test1@test1.com
</pre>
</div><br><br>

Now click the button. If you're connected to your server, you should the following printed out:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">1
test1
test1
test1@test1.com
</pre>
</div><br><br>

(If you're getting database errors, make sure you have your configure.php file in the right place, as explained in a previous section.)
So the ID from the returned row is 1, the username and password are both test1, and the email address is test1@test1.com. Now let's look at the PHP code.<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">&lt;?PHP
$email = "";
if (isset($_POST['Submit1'])) {
require '../configure.php';
$email = $_POST['email'];
$database = "membertest";
$db_found = new mysqli(DB_SERVER, DB_USER, DB_PASS, $database );
if ($db_found) {
$SQL = $db_found-&gt;prepare('SELECT * FROM members WHERE email = ?');
$SQL-&gt;bind_param('s', $email);
$SQL-&gt;execute();
$result = $SQL-&gt;get_result();
if ($result-&gt;num_rows &gt; 0) {
while ( $db_field = $result-&gt;fetch_assoc() ) {
print $db_field['ID'] . "&lt;BR&gt;";
print $db_field['username'] . "&lt;BR&gt;";
print $db_field['password'] . "&lt;BR&gt;";
print $db_field['email'] . "&lt;BR&gt;";
}
}
else {
print "No records found";
}
$SQL-&gt;close();
$db_found-&gt;close();
}
else {
print "Database NOT Found ";
}
}
?&gt;

</pre>
</div><br><br>

The first line to examine is this one:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $email = $_POST['email'];</pre></div><br><br>

This just get the text from the textbox on the form. But notice that we're not doing any error checking here to see if it is a valid email address, or that the user hasn't entered anything malicious. (You can do error checking here, if you want. But we'll keep it simple so as not to overcomplicate the code. We're going to be using a prepared statement, so any malicious SQL that has been added will get converted into a string, not a SQL statement.)<br><br>

The next few lines set up the database username and password, as well as the server and database name. Then we have this line:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $db_found = new mysqli(DB_SERVER, DB_USER, DB_PASS, $database );</pre></div><br><br>
This is a new way to connect to the database and server. Previously we used two steps: mysqli_connect, and mysqli_select. Now, we're just using one step: mysqli. In between the round brackets of mysqli, we have four things: the server name, the username, the password, and the name of the database. (The first three are coming from the required file, configure.php. You saw how to set this up in a previous lesson.) Notice the new keyword after the equal sign. You need this to set up a new database object.<br>
Inside of an IF statement, we then have this line:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL = $db_found-&gt;prepare('SELECT * FROM members WHERE email = ?');</pre></div><br><br>
This is the first line of our prepared statement. We're preparing the SQL that we want to execute on the database table. To the right of an equal sign, we have the name of our database object, $db_found. Because it's an object you need two symbols without any space between them: a dash (-) and greater than symbol (&gt;). Next comes the inbuilt function prepare. In between the round brackets of prepare, you need to type your SQL. We're selecting all the records WHERE a certain condition is met. The curious bit is this:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> WHERE email = ?</pre></div><br><br>
The question mark is a placeholder, often called a parameter. The email part is the name of a field in our database table. The placeholder, that ?, is going to be replaced with an actual value. We do this on the next line:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL-&gt;bind_param('s', $email);</pre></div><br><br>
Notice that the $SQL variable is now an object, which is why it has the -&gt; symbols after it. Then we have an inbuilt function called bind_param. As its name suggests, this is used to bind values to those parameters you set up in the prepare function. Between the round brackets of bind_param we have a single letter s surrounded by single quotes. After a comma goes the value you want to bind, in our case this is the value we got from the textbox on the form - the email address. (The variable name doesn't have to match the field name in your database.)<br>
You can bind to more than one value. You may, for example want to check a username as well as the email address. In which case, your prepare function might look like this:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL = $db_found-&gt;prepare('SELECT * FROM members WHERE email = ? AND username = ?');</pre></div><br><br>
Notice we have an AND part separating the two fields email and username. Each field has its own equal sign and question mark placeholder<br>
The bind_param function would then be this:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL-&gt;bind_param('ss', $email, $user);</pre></div><br><br>
We have two letter s's now, one for each of the parameters. The $email and $user would be variables that we get from the HTML form.<br>
The 's' is short for string. You may have fields in your database that are numerical, in which case you need and i (for Integer) or d (for double):<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL-&gt;bind_param('i', $some_integer_value);
$SQL-&gt;bind_param('d', $some_double_value);
</pre></div><br><br>
There's also a 'b', which is short for BLOB:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL-&gt;bind_param('b', $some_blob_value);
</pre></div><br><br>
Once you have bound your parameters, you can go ahead and execute:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $SQL-&gt;execute();
</pre></div><br><br>
To get some results back, you need this:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style=""> $result = $SQL-&gt;get_result();
</pre></div><br><br>
The inbuilt function now is get_result. This will return an array of rows from your table.
You can test to see if any row were returned:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">if ($result-&gt;num_rows &gt; 0) {
}
</pre></div><br><br>
Here, we use the inbuilt function num_rows. We're testing to see if it's greater than zero. If it is, we have this:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">while ( $db_field = $result-&gt;fetch_assoc() ) {
print $db_field['ID'] . "&lt;BR&gt;";
print $db_field['username'] . "&lt;BR&gt;";
print $db_field['password'] . "&lt;BR&gt;";
print $db_field['email'] . "&lt;BR&gt;";
}
</pre></div><br><br>
We're using a while loop to loop round the returned array. The array can be traversed with:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">$db_field = $result-&gt;fetch_assoc()
</pre></div><br><br>
The $db_field is just a variable name. You can call it almost anything you like. But $result-&gt;fetch_assoc( )places each row from your database in the variable. You can then do something with each row:<br>
We're using a while loop to loop round the returned array. The array can be traversed with:<br>
<div  class="code" 
    data-ace-editor-allow-execution="true" data-ace-editor-hide-vars="false" 
    data-ace-editor-script-name="page.php" data-ace-editor-default-get="" data-ace-editor-default-post="">
<pre class="editor" 
<pre class="prettyprint notranslate prettyprinted" style="">print $db_field['ID'] . "&lt;BR&gt;";
print $db_field['username'] . "&lt;BR&gt;";
print $db_field['password'] . "&lt;BR&gt;";
print $db_field['email'] . "&lt;BR&gt;";
</pre></div><br><br>

We're just printing out each row.

<script type='text/javascript' src='../lessoncss/libs/ace/ace.js'></script>
<script type='text/javascript' src='../lessoncss/libs/FileSaver.js'></script>
<script type='text/javascript' src='..lessoncss/js/php-einfach-online-php-editor.js'></script>

<script>
jQuery('div[data-ace-editor-id]').each(function() {
    var url='http://execute.php-einfach.de:9999/execute.php'; 
    //var url='proxy.php'; // In case the browser supports cross domain requests, you can use this proxy script to forward the requests over your own server
    var language = 'en'; //Choose 'de' for German
    new OnlinePHPEditor(this, language, url);
});
</script>
</div>

    

            
        </div>
    </div>

     
     <!-- jQuery CDN - Slim version (=without AJAX) -->
    <!--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <!-- jQuery Custom Scroller CDN -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.concat.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#sidebar").mCustomScrollbar({
                theme: "minimal"
            });

            $('#sidebarCollapse').on('click', function () {
                $('#sidebar, #content').toggleClass('active');
                $('.collapse.in').toggleClass('in');
                $('a[aria-expanded=true]').attr('aria-expanded', 'false');
            });
        });
    </script>
     <div>
    <h1 style="color: black; position: absolute; right: 130px; top: 10px;">Videos</h1>
<video style="width:320px; height:220px; position:relative; right: -1020px; top:50px;" controls >
  <source src="../lessoncss/vid5.1.mp4" type="video/mp4">
  </video>
</div>
</body>
</html>
