@extends('layouts.app')

@section('content')
    <h3 class="page-title">Lessons</h3>
    
    {!! Form::model($lesson, ['method' => 'PUT', 'route' => ['lessons.update', $lesson->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.edit')
        </div>

        <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('lesson_text', 'Lesson text*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('lesson_text', old('lesson_text'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('lesson_text'))
                        <p class="help-block">
                            {{ $errors->first('lesson_text') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('code_snippet', 'Code snippet', ['class' => 'control-label']) !!}
                    {!! Form::textarea('code_snippet', old('code_snippet'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('code_snippet'))
                        <p class="help-block">
                            {{ $errors->first('code_snippet') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

