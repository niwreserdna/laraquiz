@extends('layouts.app')

@section('content')
    <h3 class="page-title">Lessons</h3>
    {!! Form::open(['method' => 'POST', 'route' => ['lessons.store']]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.create')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('topic_id', 'Chapter*', ['class' => 'control-label']) !!}
                    {!! Form::select('topic_id', $topics, old('topic_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('topic_id'))
                        <p class="help-block">
                            {{ $errors->first('topic_id') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="col-xs-12 form-group">
                    {!! Form::label('subtopic_id', 'Topic*', ['class' => 'control-label']) !!}
                    {!! Form::select('subtopic_id', $subtopics, old('subtopic_id'), ['class' => 'form-control']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('subtopic_id'))
                        <p class="help-block">
                            {{ $errors->first('subtopic_id') }}
                        </p>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('lesson_text', 'Lesson text*', ['class' => 'control-label']) !!}
                    {!! Form::textarea('lesson_text', old('lesson_text'), ['class' => 'form-control ', 'placeholder' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('lesson_text'))
                        <p class="help-block">
                            {{ $errors->first('lesson_text') }}
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>


    {!! Form::submit(trans('quickadmin.save'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

