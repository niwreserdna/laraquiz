@extends('layouts.app')

@section('content')
    <h3 class="page-title">Topics</h3>

    <p>
        <a href="{{ route('subtopics.create') }}" class="btn btn-success">@lang('quickadmin.add_new')</a>
    </p>

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.list')
        </div>

        <div class="panel-body">
            <table class="table table-bordered table-striped {{ count($subtopics) > 0 ? 'datatable' : '' }} dt-select">
                <thead>
                    <tr>
                        <th style="text-align:center;"><input type="checkbox" id="select-all" /></th>
                        <th>Chapter</th>
                        <th>Topic</th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                </thead>
                
                <tbody>
                    @if (count($subtopics) > 0)
                        @foreach ($subtopics as $subtopic)
                            <tr data-entry-id="{{ $subtopic->id }}">
                                <td></td>
                                <td>{{ $subtopic->topic_id }}</td>
                                <td>{{ $subtopic->title }}</td>
                                <td>
                                    <a href="{{ route('subtopics.show',[$subtopic->id]) }}" class="btn btn-xs btn-primary">@lang('quickadmin.view')</a>
                                    <a href="{{ route('subtopics.edit',[$subtopic->id]) }}" class="btn btn-xs btn-info">@lang('quickadmin.edit')</a>
                                    {!! Form::open(array(
                                        'style' => 'display: inline-block;',
                                        'method' => 'DELETE',
                                        'onsubmit' => "return confirm('".trans("quickadmin.are_you_sure")."');",
                                        'route' => ['subtopics.destroy', $subtopic->id])) !!}
                                    {!! Form::submit(trans('quickadmin.delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="3">@lang('quickadmin.no_entries_in_table')</td>
                        </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        window.route_mass_crud_entries_destroy = '{{ route('subtopics.mass_destroy') }}';
    </script>
@endsection